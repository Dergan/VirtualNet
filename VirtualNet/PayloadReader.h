/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#ifndef PAYLOADREADER_H
#define PAYLOADREADER_H

#include "GlobalSettings.h"
#include <Windows.h>
#include "BitConverter.h"
#include <vector>
#include <iostream>
using namespace std;

class PayloadReader
{
public:
	DWORD index;
	int _size;
	u_char * _payload;
	char * LastString;
	DWORD LastStringSize;

	PayloadReader(u_char * payload, const int size);
	u_char ReadByte();
	void ReadBytes(char * dest, int length);
	short ReadInt16();
	int ReadInt32();
	long ReadInt64();
	char* ReadString();
};

#endif