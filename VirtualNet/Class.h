/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#ifndef CLASS_H
#define CLASS_H

#include "Method.h"
#include <vector>
using namespace std;

class Class
{
public:
	bool isCCtorInitialized;
	bool isCtorInitialized;
	char * Name;
	char * FullName;
	char * NameSpace;
	vector<Method*> * Methods;
	vector<Method*> * Constructors;
	Method * StaticConstructor;
	vector<Field*> * Fields;
	Class();
	~Class();
	Class * Clone();
};

#endif