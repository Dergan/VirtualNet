/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include "GlobalSettings.h"
#include "Opcode.h"

class Instruction
{
public:
	int offset;
	Opcode * opcode;
	char * operand;
	int OffsetTarget; //used for BR.S like JMP
	int OffsetTargetIndex;

	//Used to call the .net dependency Class/methods
	char * Call_Location;
	char * Call_Name;
	char * Call_ReturnType;

	Instruction();
	~Instruction();
	Instruction(int _offset, Opcode * _opcode, char * _operand);
};

#endif