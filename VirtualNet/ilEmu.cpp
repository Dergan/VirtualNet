/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#include "ilEmu.h"

ilEmu::ilEmu(Method * m, Class * c, vector<Class*> * vclasses)
{
	method = m;
	MainClass = c;
	stack = new Stack(m->MaxStack);
	VirtualClasses = vclasses;
	//PolyMethod polyMethod((PolyMethod::func2*)&ilEmu::ExecuteIlCode);
}

ilEmu::~ilEmu()
{

}

Stack* ilEmu::ExecuteIlCode(Stack * args)
{
	#ifdef DEBUG
	{
		cout << "Emulating method: " << method->Name << "\r\n";
	}
	#endif

	if(Method::PInvokeImpl == (Method::PInvokeImpl & method->attribute))
	{
		HMODULE lib = LoadLibraryA(method->Pinvoke_Module);
		if(lib == 0)
		{
			throw exception("VirtualNet, Module could not be found.");
		}
		DWORD_PTR func = (DWORD_PTR)GetProcAddress(lib, method->Pinvoke_EntryPoint);
		if(func == 0)
		{
			throw exception("VirtualNet, Method could not be found.");
		}
		LPVOID ReturnValue;

		for(int i = args->CurSize() - 1; i >= 0; i--)
		{
			LPVOID val = (LPVOID)args->GetValue(i);
			__asm push eax
		}

		__asm
		{
			call [func]
			mov [ReturnValue], eax
		}

		FreeLibrary(lib);
		Stack * ret = new Stack(-1);

		if(method->CanReturn)
			ret->Push(ReturnValue);
		return ret;
	}

	for (int i = 0; i < method->Instructions->size(); i++)
	{
		Instruction * instruction = method->Instructions->at(i);

		#ifdef DEBUG
		{
			cout << "L_0" << hex << instruction->offset << "   ";
			cout << Opcode::CodeToString(instruction->opcode->code);
			if(instruction->operand != 0)
			{
				if(instruction->OffsetTarget > 0)
				{
					cout << ", " << instruction->OffsetTarget;
				}
				else
				{
					cout << ", " << instruction->operand;
				}
			}
			cout << "\r\n";
		}
		#endif

		switch(instruction->opcode->code)
		{
			#pragma region CALL
			case Opcode::Call:
			{
				Method * m = new Method();
				Class * c = new Class();
				if(FindMethod(instruction, *c, *m))
				{
					#ifdef DEBUG
					{
						cout << "\r\n";
					}
					#endif

					ilEmu * emu = new ilEmu(m, c, VirtualClasses);
					Stack* ret = emu->ExecuteIlCode(stack);
					stack->Clear();//after a method is called the stack is being cleared
					if(emu->method->CanReturn)
					{
						stack->Push(ret->PopFirst());
					}

					#ifdef DEBUG
					{
						cout << "Continueing with method: " << method->Name << "\r\n";
					}
					#endif
				}
				else
				{
					stack->Clear(); //clear stack for now
					//throw exception("[Interpreter] Unable to find method to execute");
				}
				break;
			}
			case Opcode::Callvirt:
			{
				#ifdef DEBUG
				{
					cout << "[Interpreter] executing method " << instruction->operand << "\r\n";
				}
				#endif

				Method * m = new Method();
				Class * c = new Class();
				if(FindMethod(instruction, *c, *m))
				{
					ilEmu * emu = new ilEmu(m, (Class*)stack->PopFirst(), VirtualClasses);
					Stack* ret = emu->ExecuteIlCode(stack);
					stack->Clear();//after a method is called the stack is being cleared
					if(emu->method->CanReturn)
					{
						stack->Push(ret->PopFirst());
					}
				}
				break;
			}
			#pragma endregion
			#pragma region XOR (value1 ^ value2)
			case Opcode::Xor:
			{
				int value1 = (int)stack->PopFirst();
				int value2 = (int)stack->PopFirst();
				int ret = value1 ^ value2;
				stack->Push((void*)ret);
				break;
			}
			#pragma endregion
			#pragma region LDC.I4
			case Opcode::Ldc_I4:
			case Opcode::Ldc_I4_S:
			{
				stack->Push((void*)atoi(instruction->operand));
				break;
			}
			case Opcode::Ldc_I4_0: { stack->Push(0); break; }
			case Opcode::Ldc_I4_1: { stack->Push((void*)1); break; }
			case Opcode::Ldc_I4_2: { stack->Push((void*)2); break; }
			case Opcode::Ldc_I4_3: { stack->Push((void*)3); break; }
			case Opcode::Ldc_I4_4: { stack->Push((void*)4); break; }
			case Opcode::Ldc_I4_5: { stack->Push((void*)5); break; }
			case Opcode::Ldc_I4_6: { stack->Push((void*)6); break; }
			case Opcode::Ldc_I4_7: { stack->Push((void*)7); break; }
			case Opcode::Ldc_I4_8: { stack->Push((void*)8); break; }
			#pragma endregion
			#pragma region STLOC (Variable[index] = Stack[0])
			case Opcode::Stloc_0: { Stloc_S(0); break; }
			case Opcode::Stloc_1: { Stloc_S(1); break; }
			case Opcode::Stloc_2: { Stloc_S(2); break; }
			case Opcode::Stloc_3: { Stloc_S(3); break; }
			#pragma endregion
			#pragma region ldloc.index
			case Opcode::Ldloc_0: { LdLoc_Index(0); break; }
			case Opcode::Ldloc_1: { LdLoc_Index(1); break; }
			case Opcode::Ldloc_2: { LdLoc_Index(2); break; }
			case Opcode::Ldloc_3: { LdLoc_Index(3); break; }
			#pragma endregion
			#pragma region Return
			case Opcode::Ret:
			{
				if(stack->CurSize() > 0 && method->CanReturn)
				{
					Stack * s = new Stack(1);
					s->Push(stack->PopFirst());
					return s;
				}
				return 0;
				break;
			}
			#pragma endregion
			#pragma region Jumps
			#pragma region Br_S
			case Opcode::Br_S:
			{
				#ifdef DEBUG
				{
					cout << "[Interpreter] jumping to offset " << instruction->OffsetTarget << "\r\n";
				}
				#endif
				i = instruction->OffsetTargetIndex;
				break;
			}
			#pragma endregion
			#pragma region Bne_Un_S
			case Opcode::Bne_Un_S:
			{
				if(stack->CurSize() < 2)
				{
					throw exception("[VirtualNet] Stack is too small");
				}
				
				bool condition = stack->PopFirst() == stack->PopFirst();

				#ifdef DEBUG
				{
					cout << "\tCondition: " << (condition ? "true" : "false") << "\r\n";
				}
				#endif

				if(!condition)
				{
					i = instruction->OffsetTargetIndex;
				}
				break;
			}
			#pragma endregion
			#pragma region Beq_S
			case Opcode::Beq_S:
			{
				bool condition = stack->PopFirst() == stack->PopFirst();

				#ifdef DEBUG
				{
					cout << "\tCondition: " << (condition ? "true" : "false") << "\r\n";
				}
				#endif

				if(condition)
				{
					i = instruction->OffsetTargetIndex;
				}
				break;
			}
			#pragma endregion
			#pragma endregion
			#pragma region Blt.S (Value1 < Value2), jump if true
			case Opcode::Blt_S:
			{
				int value1 = (int)stack->PopFirst();
				int value2 = (int)stack->PopFirst();

				if(value1 < value2)
					i = instruction->OffsetTargetIndex;
				break;
			}
			#pragma endregion
			#pragma region Cgt, If(value1 < value2), value to stack
			case Opcode::Cgt:
			{
				int value1 = (int)stack->PopFirst();
				int value2 = (int)stack->PopFirst();
				int ret = value1 < value2;
				stack->Push((void*)ret);
				break;
			}
			#pragma endregion
			#pragma region Ceq, If(value1 == value2)
			case Opcode::Ceq:
			{
				int value1 = (int)stack->PopFirst();
				int value2 = (int)stack->PopFirst();
				int ret = value1 == value2;
				stack->Push((void*)ret);
				break;
			}
			#pragma endregion
			#pragma region BrFalse.S, if(Stack[0] == false)
			case Opcode::Brfalse_S:
			{
				if(stack->PopFirst() == 0)
					i = instruction->OffsetTargetIndex;
				break;
			}
			#pragma endregion
			#pragma region BrTrue.S, if(Stack[0] == true)
			case Opcode::Brtrue_S:
			{
				if(stack->PopFirst() != 0)
					i = instruction->OffsetTargetIndex;
				break;
			}
			#pragma endregion
			#pragma region BrTrue, if(Stack[0] == true)
			case Opcode::Brtrue:
			{
				if(stack->PopFirst() != 0)
					i = instruction->OffsetTargetIndex;
				break;
			}
			#pragma endregion
			#pragma region Beq, if(Value1 == Value2)
			case Opcode::Beq:
			{
				void* value1 = stack->PopFirst();
				void* value2 = stack->PopFirst();

				if(value1 == value2)
					i = instruction->OffsetTargetIndex;
				break;
			}
			#pragma endregion
			#pragma region LDarg.index
			case Opcode::Ldarg_0: { LDarg_Index(0, args); break; }
			case Opcode::Ldarg_1: { LDarg_Index(1, args); break; }
			case Opcode::Ldarg_2: { LDarg_Index(2, args); break; }
			case Opcode::Ldarg_3: { LDarg_Index(3, args); break; }
			#pragma endregion
			#pragma region Pop
			case Opcode::Pop:
			{
				stack->PopFirst();
				break;
			}
			#pragma endregion
			#pragma region LdStr
			case Opcode::Ldstr:
			{
				//don't know for sure if we should look in metadata
				stack->Push(instruction->operand);
				break;
			}
			#pragma endregion
			#pragma region Newobj
			case Opcode::Newobj:
			{
				Class * c = new Class();
				Method * m = new Method();
				if(FindMethod(instruction, *c, *m))
				{
					if(c->StaticConstructor->Instructions->size() > 0)
						ilEmu(c->StaticConstructor, c, VirtualClasses).ExecuteIlCode(stack);

					ilEmu * emu = new ilEmu(m, c, VirtualClasses);
 					Stack * ret = emu->ExecuteIlCode(stack);
					delete emu;
					delete ret;

					//in msdn documentation we should add this to this before newobj but we do it after
					c = (Class*)c->Clone();
					stack->Push(c);

					#ifdef DEBUG
					{
						cout << "\tinitialized the class...\r\n";
					}
					#endif
				}
				break;
			}
			#pragma endregion
			#pragma region Stfld
			case Opcode::Stfld:
			{
				Field * field = (Field*)stack->PopFirst();
				void* value = stack->PopFirst();
				Field * f = GetField(instruction->operand);
				f->value = value;
				#ifdef DEBUG
				{
					cout << "\tnew variable value \"" << f->Name << "\" value:" << f->value << "\r\n";
				}
				#endif
				break;
			}
			#pragma endregion
			#pragma region Add
			case Opcode::Add:
			{
				int val1 = (int)stack->PopFirst();
				int val2 = (int)stack->PopFirst();
				stack->Push((void*)(val2 + val1));
				break;
			}
			#pragma endregion
			#pragma region And
			case Opcode::And:
			{
				int val1 = (int)stack->PopFirst();
				int val2 = (int)stack->PopFirst();
				stack->Push((void*)(val2 & val1));
				break;
			}
			#pragma endregion
			default:
			{
				if(instruction->opcode->code != 0) //0 = NOP
				{
					#ifdef DEBUG
					{
						cout << "[Interpreter] unknown opcode id: " << instruction->opcode->code << "\r\n";
						__asm int 3
					}
					#else
					{
						throw exception("[VirtualNet] Core crash, unknown opcode id");
					}
					#endif
				}
				break;
			}
		}
	}
	return 0;
}

void ilEmu::Stloc_S(int VariableIndex)
{
	if(method->LocalVariables->size() < VariableIndex)
	{
		throw exception("[VirtualNet] Variable does not exist");
	}

	method->LocalVariables->at(VariableIndex)->Value = stack->PopFirst();
	#ifdef DEBUG
	{
		cout << "\tVariable: " << method->LocalVariables->at(VariableIndex)->Name << ", new value:" << method->LocalVariables->at(VariableIndex)->Value << "\r\n";
	}
	#endif
}

void ilEmu::LdLoc_Index(int index)
{
	if(method->LocalVariables->size() < index)
	{
		throw exception("[VirtualNet] Variable does not exist");
	}
	stack->Push(method->LocalVariables->at(index)->Value);
}

bool ilEmu::FindMethod(Instruction * inst, Class& retClass, Method& retMethod)
{
	for(int i = 0; i < VirtualClasses->size(); i++)
	{
		Class * c = VirtualClasses->at(i);
		
		for(int x = 0; x < c->Methods->size(); x++)
		{
			Method * m = c->Methods->at(x);
			if(strcmp(inst->operand, m->FullName) == 0)
			{
				retMethod = *m;
				retClass = *c;
				return true;
			}
		}
	}
	return false;
}

void ilEmu::LDarg_Index(int index, Stack * args)
{
	if(args->CurSize() < (index == 0 ? index+1 : index))
		throw exception("[VirtualNet] Argument does not exist");
	if(this->method->HasThis && index > 0)
		stack->Push(args->GetValue(index-1)); //fix the index
	else
		stack->Push(args->GetValue(index));
}

Field * ilEmu::GetField(char * FullName)
{
	for(int i = 0; i < MainClass->Fields->size(); i++)
	{
		Field * f = MainClass->Fields->at(i);
		if(strcmp(f->FullName, FullName) == 0)
		{
			return f;
		}
	}

	throw exception("[VirtualNet] A field does not exist");
	return 0;
}