/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#ifndef POLY_METHOD_H
#define POLY_METHOD_H

class PolyMethod
{
public:
	int Length;
	void* TargetFunc;
	void* OrgFunc;
	typedef void*(*func1)();
	typedef void*(*func2)(void*);
	typedef void*(*func3)(void*, void*);

	PolyMethod(func2 func);
	~PolyMethod();
};

#endif