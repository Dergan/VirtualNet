/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#ifndef FIELD_H
#define FIELD_H

class Field
{
public:
	enum FieldAttributes
	{
		FieldAccessMask		= 0x0007,
		Compilercontrolled	= 0x0000,	// Member not referenceable
		Private				= 0x0001,	// Accessible only by the parent type
		FamANDAssem			= 0x0002,	// Accessible by sub-types only in this assembly
		Assembly			= 0x0003,	// Accessible by anyone in the Assembly
		Family				= 0x0004,	// Accessible only by type and sub-types
		FamORAssem			= 0x0005,	// Accessible by sub-types anywhere, plus anyone in the assembly
		Public				= 0x0006,	// Accessible by anyone who has visibility to this scope field contract attributes

		Static				= 0x0010,	// Defined on type, else per instance
		InitOnly			= 0x0020,	// Field may only be initialized, not written after init
		Literal				= 0x0040,	// Value is compile time constant
		NotSerialized		= 0x0080,	// Field does not have to be serialized when type is remoted
		SpecialName			= 0x0200,	// Field is special

		// Interop Attributes
		PInvokeImpl			= 0x2000,	// Implementation is forwarded through PInvoke

		// Additional flags
		RTSpecialName		= 0x0400,	// CLI provides 'special' behavior, depending upon the name of the field
		HasFieldMarshal		= 0x1000,	// Field has marshalling information
		HasDefault			= 0x8000,	// Field has default
		HasFieldRVA			= 0x0100	 // Field has RVA
	};

	char * Name;
	char * DeclareType;
	FieldAttributes Attributes;
	int offset;
	bool IsStatic;
    bool IsSpecialName;
    bool IsRuntimeSpecialName;
    bool IsLiteral;
    bool HasConstant;
	int InitialValueLength;
	char * InitialValue;
	void * value;
	char * FullName;

	Field(char * _Name, char * _DeclareType, FieldAttributes _Attributes, int _offset,
		  bool _IsStatic, bool _IsSpecialName, bool _IsRuntimeSpecialName,
		  bool _IsLiteral, bool HasConstant, int _InitialValueLength, char * _InitialValue, char * fullName);
	Field();
	~Field();
	bool isValueSet();
};

#endif