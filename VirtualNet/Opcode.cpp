/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#include "Opcode.h"

Opcode::Opcode(Code _code)
{
	code = _code;
}

char* Opcode::CodeToString(Opcode::Code code)
{
#ifdef DEBUG
	switch(code)
	{
		case Opcode::Nop: return "nop";
		case Opcode::Call: return "call";
		case Opcode::Callvirt: return "callvirt";
		case Opcode::Ldstr: return "ldstr";
		case Opcode::Ldc_I4: return "ldc.i4";
		case Opcode::Ldc_I4_0: return "ldc.i4.0";
		case Opcode::Ldc_I4_1: return "ldc.i4.1";
		case Opcode::Ldc_I4_2: return "ldc.i4.2";
		case Opcode::Ldc_I4_3: return "ldc.i4.3";
		case Opcode::Ldc_I4_4: return "ldc.i4.4";
		case Opcode::Ldc_I4_5: return "ldc.i4.5";
		case Opcode::Ldc_I4_6: return "ldc.i4.6";
		case Opcode::Ldc_I4_7: return "ldc.i4.7";
		case Opcode::Ldc_I4_8: return "ldc.i4.8";
		case Opcode::Ldc_I4_M1: return "ldc.i4.m1";
		case Opcode::Ldc_I4_S: return "ldc.i4.S";
		case Opcode::Br: return "br";
		case Opcode::Beq_S: return "beq.s";
		case Opcode::Brfalse: return "br.false";
		case Opcode::Brfalse_S: return "br.false.S";
		case Opcode::Brtrue: return "br.true";
		case Opcode::Brtrue_S: return "br.true.S";
		case Opcode::Br_S: return "br.S";
		case Opcode::Bne_Un_S: return "bne.un_s";
		case Opcode::Bne_Un: return "bne.un";
		case Opcode::Ldloc_0: return "ldloc.0";
		case Opcode::Ldloc_1: return "ldloc.1";
		case Opcode::Ldloc_2: return "ldloc.2";
		case Opcode::Ldloc_3: return "ldloc.3";
		case Opcode::Ldloc_S: return "ldloc.S";
		case Opcode::Ldloca: return "ldloc.A";
		case Opcode::Ldloca_S: return "ldloc.S";
		case Opcode::Stloc: return "stloc";
		case Opcode::Stloc_0: return "stloc.0";
		case Opcode::Stloc_1: return "stloc.1";
		case Opcode::Stloc_2: return "stloc.2";
		case Opcode::Stloc_3: return "stloc.3";
		case Opcode::Stloc_S: return "stloc.S";
		case Opcode::Add: return "add";
		case Opcode::Ret: return "ret";
		case Opcode::Clt: return "clt";
		case Opcode::Xor: return "xor";
		case Opcode::Pop: return "pop";
		case Opcode::Ldarg_0: return "ldarg.0";
		case Opcode::Ldarg_1: return "ldarg.1";
		case Opcode::Ldarg_2: return "ldarg.2";
		case Opcode::Ldarg_3: return "ldarg.3";
		case Opcode::Cgt: return "cgt";
		case Opcode::Ceq: return "ceq";
		case Opcode::Blt_S: return "blt.s";
		case Opcode::Newobj: return "newobj";
		case Opcode::Stfld: return "stfld";
		case Opcode::Newarr: return "newarr";
		default:
		{
			return "unknown";
		}
	}
#endif
	return "";
}