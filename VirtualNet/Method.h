/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#ifndef METHOD_H
#define METHOD_H

#include "GlobalSettings.h"
#include "Instruction.h"
#include "Field.h"
#include "Variable.h"
#include <Windows.h>
#include <iostream>
#include <vector>
using namespace std;

class Method
{
public:
	enum MethodAttributes
	{
		MemberAccessMask	= 0x0007,
		Compilercontrolled	= 0x0000,	// Member not referenceable
		Private				= 0x0001,	// Accessible only by the parent type
		FamANDAssem			= 0x0002,	// Accessible by sub-types only in this Assembly
		Assem				= 0x0003,	// Accessibly by anyone in the Assembly
		Family				= 0x0004,	// Accessible only by type and sub-types
		FamORAssem			= 0x0005,	// Accessibly by sub-types anywhere, plus anyone in assembly
		Public				= 0x0006,	// Accessibly by anyone who has visibility to this scope

		Static				= 0x0010,	// Defined on type, else per instance
		Final				= 0x0020,	// Method may not be overridden
		Virtual				= 0x0040,	// Method is virtual
		HideBySig			= 0x0080,	// Method hides by name+sig, else just by name

		VtableLayoutMask	= 0x0100,	// Use this mask to retrieve vtable attributes
		ReuseSlot			= 0x0000,	// Method reuses existing slot in vtable
		NewSlot				= 0x0100,	// Method always gets a new slot in the vtable

		Abstract			= 0x0400,	// Method does not provide an implementation
		SpecialName			= 0x0800,	// Method is special

		// Interop Attributes
		PInvokeImpl			= 0x2000,	// Implementation is forwarded through PInvoke
		UnmanagedExport		= 0x0008,	// Reserved: shall be zero for conforming implementations

		// Additional flags
		RTSpecialName		= 0x1000,	// CLI provides 'special' behavior, depending upon the name of the method
		HasSecurity			= 0x4000,	// Method has security associate with it
		RequireSecObject	= 0x8000	 // Method calls another method containing security code
	};

	vector<Instruction*> * Instructions;
	vector<Field*> * Fields;
	vector<Variable*> * LocalVariables;

	char * Name;
	char * FullName;
	bool IsConstructor;
	char * NameSpace;
	MethodAttributes attribute;
	bool isEntryPoint;
	char * ReturnType;
	int MaxStack;
	bool CanReturn;
	bool HasThis;

	//pinvoke information
	char * Pinvoke_EntryPoint;
	char * Pinvoke_Module;

	Method();
	~Method();
};

#endif