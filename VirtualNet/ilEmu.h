/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#ifndef ILEMU_H
#define ILEMU_H

#include <Windows.h>
#include "Method.h"
#include "Class.h"
#include "Stack.h"
#include "PolyMethod.h"
#include <vector>
using namespace std;

class ilEmu
{
public:
	Method * method;
	Class * MainClass;
	Stack * stack;
	vector<Class*> * VirtualClasses;

	ilEmu(Method * m, Class * c, vector<Class*> * vclasses);
	~ilEmu();
	Stack* ExecuteIlCode(Stack * args);
	void Stloc_S(int VariableIndex);
	void LdLoc_Index(int index);
	bool FindMethod(Instruction * inst, Class& retClass, Method& retMethod);
	void LDarg_Index(int index, Stack * args);
	Field * GetField(char * FullName);
};

#endif