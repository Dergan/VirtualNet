/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#include "Field.h"

Field::Field(char * _Name, char * _DeclareType, FieldAttributes _Attributes, int _offset,
				   bool _IsStatic, bool _IsSpecialName, bool _IsRuntimeSpecialName,
				   bool _IsLiteral, bool _HasConstant, int _InitialValueLength, char * _InitialValue, char * fullName)
{
	Name = _Name;
	DeclareType = _DeclareType;
	Attributes = _Attributes;
	offset = _offset;
	IsStatic = _IsStatic;
	IsSpecialName = _IsSpecialName;
	IsRuntimeSpecialName = _IsRuntimeSpecialName;
	IsLiteral = _IsLiteral;
	HasConstant = _HasConstant;
	InitialValueLength = _InitialValueLength;
	InitialValue = _InitialValue;
	value = 0;
	FullName = fullName;
}

Field::Field()
{
	Name = "";
	DeclareType = "";
	offset = 0;
	IsStatic = false;
	IsSpecialName = false;
	IsRuntimeSpecialName = false;
	IsLiteral = false;
	HasConstant = false;
	InitialValueLength = 0;
	InitialValue = 0;
}

Field::~Field()
{

}

bool Field::isValueSet()
{
	return value != 0;
}