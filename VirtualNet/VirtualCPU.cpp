/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#include "VirtualCPU.h"

VirtualCPU::VirtualCPU(char code[], int size)
{
	//Speed_IPS = 0;
	SourceCode = code;
	CodeSize = size;
	Classes = new vector<Class*>();
}

VirtualCPU::~VirtualCPU()
{
}

void VirtualCPU::InitializeCode()
{
	pr = new PayloadReader((u_char*)SourceCode, CodeSize);

	int ClassCount = pr->ReadInt32();
	for(int i = 0; i < ClassCount; i++)
	{
		Class * c = new Class();
		ReadClass(c);
		Classes->push_back(c);
	}

	/*int DotNetDependencyCount = pr->ReadInt32();
	for(int i = 0; i < DotNetDependencyCount; i++)
	{
		Class * c = new Class();
		ReadClass(c);
		Classes->push_back(c);
	}*/
}

void VirtualCPU::ReadClass(Class * c)
{
	c->Name = pr->ReadString();
	c->FullName = pr->ReadString();
	c->NameSpace = pr->ReadString();
	int MethodCount = pr->ReadInt32();
	int ConstructorCount = pr->ReadInt32();
	int VariableCount = pr->ReadInt32();
	int PropertyCount = pr->ReadInt32();
		
	for(int x = 0; x < MethodCount; x++)
	{
		Method * m = new Method();
		ReadMethod(m);
		if(m->isEntryPoint)
		{
			EntryClass = c;
		}
		c->Methods->push_back(m);
	}
	for(int x = 0; x < ConstructorCount; x++)
	{
		Method * m = new Method();
		ReadMethod(m);

		if(m->Name == ".cctor")
			c->StaticConstructor = m;

		c->Constructors->push_back(m);
	}
	for(int x = 0; x < VariableCount; x++)
	{
		Field * var = new Field();
		ReadField(var);
		c->Fields->push_back(var);
	}
}

void VirtualCPU::ReadField(Field * var)
{
	var->Name = pr->ReadString();
	var->DeclareType = pr->ReadString();
	var->Attributes = (Field::FieldAttributes)pr->ReadInt32();
	var->offset = pr->ReadInt32();
	var->IsStatic = pr->ReadByte() == 1;
	var->IsSpecialName = pr->ReadByte() == 1;
	var->IsRuntimeSpecialName = pr->ReadByte() == 1;
	var->IsLiteral = pr->ReadByte() == 1;
	var->HasConstant = pr->ReadByte() == 1;
	var->InitialValueLength = pr->ReadInt32();
	if(var->InitialValueLength > 0)
	{
		var->InitialValue = new char[var->InitialValueLength];
		pr->ReadBytes(var->InitialValue, var->InitialValueLength);
	}
	var->FullName = pr->ReadString();
}

void VirtualCPU::ReadMethod(Method * method)
{
	method->Name = pr->ReadString();
	method->NameSpace = pr->ReadString();
	method->IsConstructor = pr->ReadByte() == 1;
	method->attribute = (Method::MethodAttributes)pr->ReadInt32();

	if(pr->ReadByte() == 1)
	{
		method->isEntryPoint = true;
		EntryPoint = method;
	}

	method->ReturnType = pr->ReadString();
	method->MaxStack = pr->ReadInt32();
	method->FullName = pr->ReadString();
	method->CanReturn = pr->ReadByte() == 1;
	method->HasThis = pr->ReadByte();

	if(pr->ReadByte() == 1)
	{
		method->Pinvoke_EntryPoint = pr->ReadString();
		method->Pinvoke_Module = pr->ReadString();
	}

	int VarCount = pr->ReadInt32();
	for(int i = 0; i < VarCount; i++)
	{
		Variable * var = new Variable();
		var->Index = pr->ReadInt32();
		var->Name = pr->ReadString();
		method->LocalVariables->push_back(var);
	}

	int InstuctionsCount = pr->ReadInt32();
	for(int i = 0; i < InstuctionsCount; i++)
	{
		Instruction * inst = new Instruction();
		if(pr->ReadByte() == 1) //has Operand ?
		{
			inst->operand = pr->ReadString();
		}

		Opcode::Code OpcodeCode = (Opcode::Code)pr->ReadInt32();
		inst->offset = pr->ReadInt32();

		int OperandType = pr->ReadByte();
		if(OperandType == 1)
		{
			inst->OffsetTarget = pr->ReadInt32();
			inst->OffsetTargetIndex = pr->ReadInt32();
		}
		else if(OperandType == 2)
		{
			inst->Call_Location = pr->ReadString();
			inst->Call_Name = pr->ReadString();
			inst->Call_ReturnType = pr->ReadString();
		}

		//lets first read the entry point
		inst->opcode = new Opcode(OpcodeCode);
		method->Instructions->push_back(inst);
	}
}

void VirtualCPU::Run()
{
	//call the static constructor at <Module> like original
	if(Classes->size() > 1)
	{
		for(int x = 0; x < Classes->at(0)->Constructors->size(); x++)
		{
			Method * m = Classes->at(0)->Constructors->at(x);
			if((m->attribute & Method::Static) == Method::Static)
			{
				ilEmu * emu = new ilEmu(m, Classes->at(0), Classes);
				emu->ExecuteIlCode(0);
				delete emu;
				break;
			}
		}
	}

	ilEmu * emu = new ilEmu(EntryPoint, EntryClass, Classes);
	emu->ExecuteIlCode(0);

#ifdef DEBUG
	cout << "\r\n[Reached end of emulation]\r\n";
#endif

#ifdef PAUSE_AT_END
		Sleep(INFINITE);
#endif
}