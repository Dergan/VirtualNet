/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#include "Class.h"

Class::Class()
{
	isCCtorInitialized = false;
	isCtorInitialized = false;
	Name = "";
	FullName = "";
	NameSpace = "";
	Methods = new vector<Method*>();
	Constructors = new vector<Method*>();
	Fields = new vector<Field*>();
	StaticConstructor = new Method();
}

Class::~Class()
{

}

Class * Class::Clone()
{
	//not the best deep clone ever... but its something
	Class * c = new Class();
	
	for(int i = 0; i < Constructors->size(); i++)
		c->Constructors->push_back(Constructors->at(i));
	for(int i = 0; i < Methods->size(); i++)
		c->Methods->push_back(Methods->at(i));
	for(int i = 0; i < Fields->size(); i++)
		c->Fields->push_back(Fields->at(i));
	c->StaticConstructor = StaticConstructor; //DEEP CLONE IT
	c->Name = Name;
	c->FullName = FullName;
	c->isCCtorInitialized = isCCtorInitialized;
	c->isCtorInitialized = isCtorInitialized;
	c->NameSpace = NameSpace;
	return c;
}