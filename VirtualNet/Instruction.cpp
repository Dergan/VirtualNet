/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#include "Instruction.h"


Instruction::Instruction(int _offset, Opcode * _opcode, char * _operand)
{
	Call_Location = "";
	Call_Name = "";
	Call_ReturnType = "";
	offset = _offset;
	opcode = _opcode;
	operand = _operand;
	OffsetTarget = -1;
	OffsetTargetIndex = -1;
}

Instruction::Instruction()
{
	Call_Location = "";
	Call_Name = "";
	Call_ReturnType = "";
	operand = 0;
	OffsetTarget = -1;
}

Instruction::~Instruction()
{
}