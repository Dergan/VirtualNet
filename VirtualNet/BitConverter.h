/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#ifndef BITCONVERTER_H
#define BITCONVERTER_H

#include "GlobalSettings.h"
#include <Windows.h>

class BitConverter
{
public:
	static int GetInt16(u_char * value, const int index);
	static int GetInt32(u_char * value, const int index);
	static int GetInt64(u_char * value, const int index);
	static void GetBytes(short value, u_char * dest);
	static void GetBytes(int value, u_char * dest);
	static void GetBytes(long value, u_char * dest);
};

#endif