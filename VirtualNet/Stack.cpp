/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#include "Stack.h"

Stack::Stack(int maxStack)
{
	stack = new vector<void*>();
	MaxStack = maxStack;
}

Stack::~Stack()
{
	stack->clear();
	delete stack;
}

void Stack::Push(void* value)
{
	if(stack->size()+1 > MaxStack && MaxStack != -1)
		throw exception("[Stack] Stack is too big");

	stack->push_back(value);
}

void Stack::Remove(int index)
{
	if(stack->size() < index)
		throw exception("[Stack] Item at index does not exist");
	stack->erase(stack->begin()+index);
}

void* Stack::PopFirst()
{
	return Pop(0);
}

void* Stack::PopLast()
{
	return Pop(stack->size() - 1);
}

void* Stack::Pop(int index)
{
	if(stack->size() < index)
		throw exception("[Stack] Stack is too small");
	void * ret = stack->at(index);
	Remove(index);
	return ret;
}

int Stack::CurSize()
{
	return stack->size();
}

void Stack::Clear()
{
	stack->clear();
}

void* Stack::GetValue(int index)
{
	if(stack->size() < index)
		throw exception("[Stack] Item at index does not exist");
	return stack->at(index);
}