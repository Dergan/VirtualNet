/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#ifndef STACK_H
#define STACK_H

#include <vector>
using namespace std;

class Stack
{
public:
	int MaxStack;
	Stack(int maxStack);
	~Stack();

	void Push(void* value);
	void Remove(int index);
	void Clear();
	void* PopFirst();
	void* PopLast();
	void* Pop(int index);
	void* GetValue(int index);
	int CurSize();
private:
	vector<void*> * stack;
};

#endif