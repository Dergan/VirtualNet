/*
VirtualNet - A ILcode interpeter
Copyright (C) <year>  <name of author>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Check us out at virtualnet.codeplex.com
Creator: DragonHunter
*/

#include "PayloadReader.h"

PayloadReader::PayloadReader(u_char * payload, const int size)
{
	_payload = new u_char[size];
	memcpy_s(_payload, size, payload, size);
	index = 0;
	_size = size;
}

u_char PayloadReader::ReadByte()
{
	u_char ret = _payload[index];
	index++;
	return ret;
}

void PayloadReader::ReadBytes(char * dest, int length)
{
	memcpy_s(dest, length, _payload+index, length);
	index += length;
}

short PayloadReader::ReadInt16()
{
	short ret = BitConverter::GetInt16(_payload, index);
	index += 2;
	return ret;
}

int PayloadReader::ReadInt32()
{
	int ret = BitConverter::GetInt32(_payload, index);
	index += 4;
	return ret;
}

long PayloadReader::ReadInt64()
{
	long ret = BitConverter::GetInt64(_payload, index);
	index += 8;
	return ret;
}

char* PayloadReader::ReadString()
{
	vector<u_char> stringy;
	int length = 0;
	for(DWORD i = index; i < _size; i++)
	{
		if(_payload[i] == 0)
			break;

		stringy.push_back((u_char)_payload[i]);
		length++;
	}

	//Copy the string directly to the LastString
	if(length > 0)
	{
		LastString = new char[length+1];
		memcpy_s(LastString, length, &stringy[0], length);
		LastString[length] = '\0';
		LastStringSize = length;
	}
	else
	{
		LastString = new char[0];
		LastStringSize = 0;
	}

	index += stringy.size() + 1;
	stringy.clear();
	return LastString;
}