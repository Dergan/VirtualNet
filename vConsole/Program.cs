﻿using System;
using System.Collections.Generic;
using System.Text;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System.Diagnostics;
using System.IO;

namespace vConsole
{
    class Program
    {
        static SortedList<string, TypeDefinition> WrittenClasses;
        static AssemblyDefinition SystemLib;
        static SortedList<string, TypeDefinition> Classes;

        static void Main(string[] args)
        {
            WrittenClasses = new SortedList<string, TypeDefinition>();
            PayloadWriter pw = new PayloadWriter();
            AssemblyDefinition asm = AssemblyDefinition.ReadAssembly(@"./VirtualizeMe.exe");
            SystemLib = AssemblyDefinition.ReadAssembly(@"C:\Windows\Microsoft.NET\Framework\v2.0.50727\mscorlib.dll");

            pw.WriteInteger(asm.MainModule.Types.Count);
            foreach(TypeDefinition c in asm.MainModule.Types)
            {
                WriteClass(c, asm, ref pw);
            }

            //reading .net dependencies...
            PayloadWriter NetPW = new PayloadWriter();
            List<string> Processed = new List<string>();
            Classes = new SortedList<string, TypeDefinition>();

            for (int i = 0; i < WrittenClasses.Values.Count; i++)
            {
                foreach (MethodDefinition m in WrittenClasses.Values[i].Methods)
                {
                    if (Processed.Contains(m.ToString()) || m.IsConstructor)
                        continue;

                    ScanDependencies(m, ref Processed, ref Classes);
                }
            }

            for (int i = 0; i < Classes.Values.Count; i++)
            {
                foreach (MethodDefinition m in Classes.Values[i].Methods)
                {
                    if (Processed.Contains(m.ToString()) || !m.IsConstructor)
                        continue;
                    ScanDependencies(m, ref Processed, ref Classes);
                }
            }

            Console.WriteLine(Classes.Count + " .net dependencies were found.");
            if (Classes.Count > 0)
            {
                foreach (TypeDefinition c in Classes.Values)
                {
                    Console.WriteLine("[.Net Dependency] " + c.FullName);
                    Console.WriteLine("\tMethods: " + c.Methods.Count);
                    for (int i = 0; i < c.Methods.Count; i++)
                        Console.WriteLine("\t\t" + c.Methods[i].ReturnType.ToString() + " " + c.Methods[i].Name);
                    Console.Write("\r\n");

                    WriteClass(c, SystemLib, ref NetPW);
                }
            }

            pw.WriteInteger(Classes.Count);
            pw.WriteBytes(NetPW.ToByteArray());

            byte[] code = pw.ToByteArray();
            string codeStr = "0x" + BitConverter.ToString(code).Replace("-", ", 0x");
            for (int i = 72; i < codeStr.Length; i += 74)
            {
                codeStr = codeStr.Insert(i, "\r\n");
            }

            Console.WriteLine("length: " + pw.Length);
            File.WriteAllText(".//codezor.txt", codeStr);
            Process.Start(Environment.CurrentDirectory + "\\codezor.txt");
            Console.WriteLine("DONE VIRTUALIZING");
            Process.GetCurrentProcess().WaitForExit();
        }

        static void ScanDependencies(MethodDefinition method, ref List<string> Processed, ref SortedList<string, TypeDefinition> Classes)
        {
            if (method.HasBody)
            {
                foreach (Instruction i in method.Body.Instructions)
                {
                    if (i.Operand == null)
                        continue;

                    Type t = i.Operand.GetType();
                    if (t == typeof(MethodReference) ||
                        t == typeof(MethodDefinition))
                    {
                        MethodDefinition m = ((MethodReference)i.Operand).Resolve();

                        if (!Processed.Contains(m.FullName))
                        {
                            Processed.Add(m.FullName);

                            TypeDefinition Class = new TypeDefinition(m.DeclaringType.Namespace, m.DeclaringType.Name, m.DeclaringType.Attributes);
                            if (Classes.ContainsKey(Class.ToString()))
                                Class = Classes[Class.ToString()];
                            else
                                Classes.Add(Class.ToString(), Class);
                            Class.Methods.Add(m.Clone());

                            //deep scan the method
                            ScanDependencies(m, ref Processed, ref Classes);
                        }
                    }
                    else if(t == typeof(FieldReference))
                    {

                    }
                }
            }
        }

        static void WriteClass(TypeDefinition Class, AssemblyDefinition asm, ref PayloadWriter pw)
        {
            if (WrittenClasses.ContainsKey(Class.FullName))
                return;

            pw.WriteString(Class.Name);
            pw.WriteString(Class.FullName);
            pw.WriteString(Class.Namespace);
            pw.WriteInteger(Class.Methods.Count);
            pw.WriteInteger(Class.Constructors.Count);
            pw.WriteInteger(Class.Fields.Count);
            pw.WriteInteger(Class.Properties.Count);

            foreach (MethodDefinition m in Class.Methods)
                WriteMethod(m, asm, ref pw);
            foreach (MethodDefinition m in Class.Constructors)
                WriteMethod(m, asm, ref pw);
            foreach (FieldDefinition field in Class.Fields)
                WriteField(field, ref pw);

            WrittenClasses.Add(Class.FullName, Class);
        }

        static void WriteMethod(MethodDefinition method, AssemblyDefinition asm, ref PayloadWriter pw)
        {
            pw.WriteString(method.Name);
            pw.WriteString(method.DeclaringType.Namespace);
            pw.WriteByte(method.IsConstructor ? (byte)1 : (byte)0);
            pw.WriteInteger((int)method.Attributes);
            pw.WriteByte((method == asm.EntryPoint) ? (byte)1 : (byte)0);
            pw.WriteString(method.ReturnType.ToString());
            pw.WriteInteger((method.Body != null) ? method.Body.MaxStackSize : 0);
            pw.WriteString(method.ToString());
            pw.WriteByte(method.ReturnType.FullName != "System.Void" ? (byte)1 : (byte)0);
            pw.WriteByte(method.HasThis ? (byte)1 : (byte)0);

            pw.WriteByte(method.IsPInvokeImpl ? (byte)1 : (byte)0);
            if (method.IsPInvokeImpl)
            {
                pw.WriteString(method.PInvokeInfo.EntryPoint);
                pw.WriteString(method.PInvokeInfo.Module.ToString());
            }

            if (method.Body != null)
            {
                pw.WriteInteger(method.Body.Variables.Count);
                foreach (VariableDefinition var in method.Body.Variables)
                    WriteVariable(var, ref pw);
            }
            else
            {
                pw.WriteInteger(0);
            }

            WriteInstructions(method, ref pw);
        }

        static void WriteVariable(VariableDefinition var, ref PayloadWriter pw)
        {
            pw.WriteInteger(var.Index);
            pw.WriteString("V_" + var.Index);
        }

        static void WriteField(FieldDefinition field, ref PayloadWriter pw)
        {
            pw.WriteString(field.Name);
            pw.WriteString(field.DeclaringType.ToString());
            pw.WriteInteger((int)field.Attributes);
            pw.WriteInteger((int)field.Offset);
            pw.WriteByte(field.IsStatic ? (byte)1 : (byte)0);
            pw.WriteByte(field.IsSpecialName ? (byte)1 : (byte)0);
            pw.WriteByte(field.IsRuntimeSpecialName ? (byte)1 : (byte)0);
            pw.WriteByte(field.IsLiteral ? (byte)1 : (byte)0);
            pw.WriteByte(field.HasConstant ? (byte)1 : (byte)0);
            pw.WriteInteger(field.InitialValue.Length);

            if(field.InitialValue.Length > 0)
                pw.WriteBytes(field.InitialValue);
            pw.WriteString(field.FullName);
        }

        static void WriteInstructions(MethodDefinition method, ref PayloadWriter pw)
        {
            if (!method.HasBody)
            {
                pw.WriteInteger(0);
                return;
            }

            pw.WriteInteger(method.Body.Instructions.Count);
            foreach(Instruction i in method.Body.Instructions)
            {
                pw.WriteByte(i.Operand == null ? (byte)0 : (byte)1);
                if(i.Operand != null)
                {
                    pw.WriteString(i.Operand.ToString());
                }

                pw.WriteInteger((int)i.OpCode.Code);
                pw.WriteInteger(i.Offset);

                if(i.Operand != null)
                {
                    if(i.Operand.GetType() == typeof(Instruction))
                    {
                        pw.WriteByte(1);
                        pw.WriteInteger(((Instruction)i.Operand).Offset);

                        int offset = -1;

                        for(int x = 0; x < method.Body.Instructions.Count; x++)
                        {
                            if (method.Body.Instructions[x].Offset == ((Instruction)i.Operand).Offset)
                            {
                                if (x > 0)
                                    offset = x - 1;
                                else
                                    offset = x;
                                break;
                            }
                        }
                        pw.WriteInteger(offset);
                    }
                    else if(i.Operand.GetType() == typeof(MethodReference))
                    {
                        //CALL information to see which function to call
                        pw.WriteByte(2);
                        pw.WriteString(((MethodReference)i.Operand).DeclaringType.ToString()); //class location
                        pw.WriteString(((MethodReference)i.Operand).Name); //function
                        pw.WriteString(((MethodReference)i.Operand).ReturnType.ToString()); //return type
                    }
                    else
                    {
                        pw.WriteByte(0);
                    }
                }
                else
                {
                    pw.WriteByte(0);
                }
            }
        }
    }
}